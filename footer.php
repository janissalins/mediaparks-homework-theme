<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package mediaparks
 */

?>
<footer>
    <div class="container">
        <ul class="social">
            <li><a href="#" target="_blank" rel="noopener noreferrer"><span class="fab fa-twitter"></span></a></li>
            <li><a href="#" target="_blank" rel="noopener noreferrer"><span class="fab fa-codepen"></span></a></li>
            <li><a href="#" target="_blank" rel="noopener noreferrer"><span class="fab fa-github"></span></a></li>
        </ul>
        <div class="site-info">
            <a href="<?php echo esc_url(__('https://wordpress.org/', 'mediaparks')); ?>">
                <?php
                printf(esc_html__('Powered by %s', 'mediaparks'), 'WordPress');
                ?>
            </a>
            <span class="sep"> | </span>
            <?php
            printf(esc_html__('Theme: %1$s by %2$s.', 'mediaparks'), 'mediaparks', 'Janis Salins');
            ?>
        </div>

    </div>
    </div>
</footer>

</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
