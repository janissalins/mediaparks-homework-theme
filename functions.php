<?php
/**
 * mediaparks functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package mediaparks
 */

if ( ! defined( '_S_VERSION' ) ) {
	// Replace the version number of the theme on each release.
	define( '_S_VERSION', '1.0.0' );
}

if ( ! function_exists( 'mediaparks_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function mediaparks_setup() {

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus(
			array(
				'menu-1' => esc_html__( 'Primary', 'mediaparks' ),
			)
		);

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
				'style',
				'script',
			)
		);

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support(
			'custom-logo',
			array(
				'height'      => 250,
				'width'       => 250,
				'flex-width'  => true,
				'flex-height' => true,
			)
		);
	}
endif;
add_action( 'after_setup_theme', 'mediaparks_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function mediaparks_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'mediaparks_content_width', 640 );
}
add_action( 'after_setup_theme', 'mediaparks_content_width', 0 );

/**
 * Enqueue scripts and styles.
 */
function mediaparks_scripts() {

	wp_enqueue_style( 'mediaparks-style', get_stylesheet_uri(), array(), _S_VERSION );
	wp_enqueue_style( 'fontawesome-style', 'https://use.fontawesome.com/releases/v5.0.12/css/all.css');

	if ( is_page_template( 'page-templates/page-homework.php' ) ) {
		wp_enqueue_script( 'mediaparks-app', get_template_directory_uri() . '/js/app.js', array(), _S_VERSION, true );
  }
}

add_action( 'wp_enqueue_scripts', 'mediaparks_scripts' );

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

