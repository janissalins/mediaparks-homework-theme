<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package mediaparks
 */
wp_enqueue_script( 'mediaparks-app' );
get_header();
?>
    <section>
        <a class="anchor" id="news"></a>
        <h1>Dribble News</h1>
      <div id="shots"></div>
    </section>

<script type="text/javascript">
    document.addEventListener('DOMContentLoaded', (event) => {
      app.loadNews();
    });
</script>
