<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package mediaparks
 */

?>

<section id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<div class="entry-content">
        <a class="anchor" id="start"></a>
		<?php the_content(); ?>
	</div><!-- .entry-content -->
</section><!-- #post-<?php the_ID(); ?> -->
