<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package mediaparks
 */

?>
<a class="anchor" id="contact"></a>
<section>
    <h1 class="text-center">Contact Me</h1>
    <div class="form-container">
        <form name="contactForm" onsubmit="event.preventDefault();">
            <div class="js-row">
                <div class="js-col ">
                    <label for="name">* Name</label>
                    <input type="text" id="name" name="name" placeholder="Your name">
                    <span class="errors"></span><br></div>
                <div class="js-col">
                    <label for="phone">* Phone</label>
                    <input type="tel" placeholder="Phone number" id="phone" name="phone">
                    <span class="errors"></span><br>
                </div>
            </div>

            <label for="email">* Email</label>
            <input type="email" placeholder="Email address" id="email" name="email">
            <span class="errors"></span><br>

            <label for="subject">* Message</label>
            <textarea placeholder="Your message" cols="132" rows="5" name="subject" id="subject"></textarea>
            <span class="errors"></span><br>

            <label class="check-container">I have read the Terms and Conditions and agree to the Privacy Policy
                <input type="checkbox" name="checkbox" id="checkbox">
                <span class="errors"></span><br>
                <span class="checkmark"></span>
            </label>

            <div id="submitButton" class="button" onclick="return app.formValidate()">Submit</div>
        </form>
    </div>
</section>