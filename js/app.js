class MyApp {

    constructor() {
        this.initMenu();
    }

    initMenu() {
        const links = document.querySelector('.navBar').querySelectorAll('li a');
        if (links) {
            links.forEach((link) => {
                link.addEventListener('click', () => {
                    this.closeMenu();
                    console.log(link.textContent, 'clicked');
                })
            })
        }
    }

    closeMenu(e) {
        const checkBox = document.getElementById('menu-toggle');
        if (checkBox) {
            checkBox.checked = false;
        }
    }

    formValidate(e) {
        const fields = document.querySelectorAll(
            '.form-container textarea, .form-container input[type="text"], .form-container input[type="tel"], .form-container input[type="email"], .form-container input[type="checkbox"]');
        let regEx;
        let removeSpan;
        let par;
        let check = false;
        let val;
        let errArr = [];
        let errShadow = "0 0 2px 1px #cc0001";
        const is_req = " is required";
        const not_valid = " is not valid";


        for (let i = 0; i < fields.length; i++) {
            if (fields[i].value === "") {

                if (fields[i].nextElementSibling.classList.contains('error')) {
                    removeSpan = fields[i].nextElementSibling;
                    par = fields[i].parentNode;
                    par.removeChild(removeSpan);
                    fields[i].nextElementSibling.innerHTML = fields[i].placeholder + is_req;
                    fields[i].style.boxShadow = errShadow;
                    ;
                    check = false;
                    errArr.push(fields[i]);
                }
                fields[i].nextElementSibling.innerHTML = fields[i].placeholder + is_req;
                fields[i].style.boxShadow = errShadow;
                ;
                check = false;
                errArr.push(fields[i]);
            } else {

                // check if message and name values contain valid characters.
                if (fields[i].id !== 'email' && fields[i].id !== 'phone') {
                    val = isValidChar(fields[i]);
                    if (val === false) {
                        fields[i].nextElementSibling.innerHTML = "Your name" + not_valid;
                        fields[i].style.boxShadow = errShadow;
                        ;
                        check = false;
                        errArr.push(fields[i]);
                    } else {
                        fields[i].nextElementSibling.innerHTML = "";
                        fields[i].style.boxShadow = "none";
                        check = true;
                    }
                }

                if (fields[i].id === 'phone') {
                    val = isValidPhone(fields[i]);
                    if (val === false) {
                        fields[i].nextElementSibling.innerHTML = "Your phone number" + not_valid;
                        fields[i].style.boxShadow = errShadow;
                        ;
                        check = false;
                        errArr.push(fields[i]);
                    } else {
                        fields[i].nextElementSibling.innerHTML = "";
                        fields[i].style.boxShadow = "none";
                        check = true;
                    }
                }

                if (fields[i].id === 'email') {
                    val = isValidEmail(fields[i]);
                    if (val === false) {
                        fields[i].nextElementSibling.innerHTML = "Your email address" + not_valid;
                        fields[i].style.boxShadow = errShadow;
                        ;
                        check = false;
                        errArr.push(fields[i]);
                    } else {
                        fields[i].nextElementSibling.innerHTML = "";
                        fields[i].style.boxShadow = "none";
                        check = true;
                    }
                }
                if (fields[i].id === 'checkbox') {
                    val = fields[i].checked;
                    if (val === false) {
                        fields[i].nextElementSibling.innerHTML = "You must agree to Terms and Conditions!";
                        fields[i].style.boxShadow = errShadow;
                        ;
                        check = false;
                        errArr.push(fields[i]);
                    } else {
                        fields[i].nextElementSibling.innerHTML = "";
                        fields[i].style.boxShadow = "none";
                        check = true;
                    }
                }
            }
        }

        if (check === false) {
            let count = 0;
            let toErr = setInterval(function () {
                let e = errArr[0].offsetTop + -25;
                let pos = Math.abs(e);
                if (count < pos) {
                    count++;
                    //window.scrollTo(0, count);
                } else {
                    clearInterval(toErr);
                }
            }, 1);
        }

        return check

        // Helper functions.
        function isValidEmail(e) {
            regEx = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
            let email = e.value;
            if (!regEx.test(email)) {
                return false;
            }
        }

        function isValidChar(e) {
            regEx = /^[a-zA-Z@#$%!?^&*()_+\-=\[\]{};':"\\|,.\/? ]*$/;
            let value = e.value;
            if (!regEx.test(value)) {
                return false;
            }
        }

        function isValidPhone(e) {
            regEx = /^[+]?[(]?[+]?\d{2,4}[)]?[-\s]?\d{2,8}[-\s]?\d{2,8}$/;
            let value = e.value;
            if (!regEx.test(value)) {
                return false;
            }
        }
    };

    buttonValidate(checkbox, btnId) {
        let btn = document.getElementById(btnId);
        if (btn) {
            !checkbox.checked ? btn.classList.add('disabled') : btn.classList.remove('disabled');
        }
    }

    loadGallery() {
        const overlay = document.querySelector('#gallery').querySelector('.overlay');
        const images  = document.querySelector('#gallery').querySelectorAll('.img-holder');
        const fullcontainer = document.querySelector('#gallery').querySelector('.img-full');
        let imgfull;
        overlay.addEventListener('click', (e) => {
            e.target.style.visibility = 'hidden';
            fullcontainer.style.visibility = 'hidden'
            if (imgfull) {
                imgfull.parentNode.removeChild(imgfull);
            }
        });
        images.forEach(image => {
            image.addEventListener('click', (e) => {
                overlay.style.visibility = 'visible';
                imgfull = document.createElement("img");
                imgfull.src = 'https://placeimg.com/640/480/any' + '/' + Math.random();
                fullcontainer.appendChild(imgfull);
                fullcontainer.style.visibility = 'visible'
            });
        })
    }

    loadNews() {
        const container = document.getElementById('shots');
        const accessToken = '9f061d26c5a8be96b17a81718959a67dd54ca9669ca41752777193f7cc5be7c3';
        const url = 'https://api.dribbble.com/v2/user/shots?access_token=' + accessToken;

        fetch(url)
            .then((resp) => resp.json())
            .then(function (data) {
                if (data.length > 0) {
                    data.slice(-4).forEach(val => {
                        let el = document.createElement("a");
                        el.classList.add('shot');
                        el.target = 'blank';
                        el.href = val.html_url;
                        el.title = val.title;
                        el.innerHTML = `<img src='${val.images.normal}'/><article><h4>${val.title}</h4><p>${val.description}</p></article>`;
                        container.appendChild(el);
                    });
                }

            })
            .catch(function (error) {
                console.log(error);
            });
    }

}

var app = new MyApp();