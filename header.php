<?php
/**
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package mediaparks
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="https://gmpg.org/xfn/11">

    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>

<nav class="navBar">
    <div class="wrapper">
        <div class="logo"></div><!-- Logo -->
        <input type="checkbox" id="menu-toggle"/>
        <label for="menu-toggle" class="label-toggle"></label>
        <ul>

            <?php
            $menu_name = 'menu-1'; //menu slug
            $locations = get_nav_menu_locations();
            $menu = wp_get_nav_menu_object($locations[$menu_name]);
            $menuitems = wp_get_nav_menu_items($menu->term_id, array('order' => 'DESC'));

            foreach ($menuitems as $item) {
                $link = $item->url;
                $title = $item->title;
                echo '<li><a href="' . $link . '">' . $title . '</a></li>';
            }

            ?>

        </ul>
    </div>
</nav>